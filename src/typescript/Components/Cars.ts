import type { ICars } from "@/interfaces/Cars/Cars";
import type { CarsApi } from "../API/CarsApi";
import type { ICar } from "@/interfaces/Cars/Car";
import type { IObserver } from "@/interfaces/Observer/Observer";
import type { ISubject } from "@/interfaces/Observer/Subject";
import { ref, type Ref } from 'vue'

export class Cars implements ICars, ISubject {
	private api: CarsApi;
	public  items: Ref<ICar[]>;
	private observers: IObserver[];
	public is_loading: boolean;
	private current_sort_option: string | null;

	constructor(Api: new(path: string) => CarsApi) {
		this.api        = new Api('/test-task/vehicles');
		this.items      = ref([]);
		this.observers  = [];
		this.is_loading = false;
		this.current_sort_option = null;
	}

	async fetchItems() {
		this.setIsLoading(true);
		
		const request = this.api.getItems();

		request.then((result) => {
			this.setItems(result);
		});

		request.catch((error) => {
			console.log(error);
		});

		request.finally(() => {
			this.setIsLoading(false);
		})
	}

	createItem(item: ICar): void {
		this.items.value = [...this.items.value, item];

		if (this.current_sort_option) {
			this.sortItems(this.current_sort_option);
		}
	}

	setIsLoading(value: boolean) {
		this.is_loading = value;
	}

	getItems() {
		return this.items.value;
	}

	setItems(cars: ICar[]) {
		this.items.value = cars;
	}

	getEmptyItem(): ICar { 
		return {
			id: '',
			name: '',
			model: '',
			year: 0,
			color: '',
			price: 0,
			latitude: 0,
			longitude: 0,
		}
	}

	removeItem(id: string | number) {
		this.items.value = this.items.value.filter(item => item.id != id);
	}

	updateItem(updated_item: ICar) {
		this.items.value = this.items.value.map((item) => {
			if (item.id == updated_item.id) {
				Object.assign(item, updated_item);
			}

			return item;
		});
	}

	sortItems<T extends ('year' | 'price')>(sort_option: string) {
		this.setCurrentSortOption(sort_option);
		const option = sort_option.split(' ')[0] as T;

		this.items.value = this.items.value.sort((a, b) => {
			if (a[option] > b[option]) {
				return sort_option.includes('desc') ? -1 : 1;
			}
			
			if (a[option] < b[option]) {
				return sort_option.includes('desc') ? 1 : -1;
			}

			return 0;
		});
	}

	setCurrentSortOption(sort_option: string) {
		this.current_sort_option = sort_option;
	}

	registerOvserver() {

	}

	removeOvserver() {

	}

	notifyOvservers() {

	}
}