import type { IApi } from "@/interfaces/API/Api"

export class CarsApi implements IApi {
	constructor(
		private path: string,
	) {}

	async getItems() {
		const request = await fetch(this.path);
		const result  = await request.json();

		return result;
	}

	async getItem() { }

	async create() { }

	async update() { }

	async delete() { }
}