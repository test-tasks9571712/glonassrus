export interface IApi {
	getItems(): Promise<any>,
	getItem(): Promise<any>,
	create(): Promise<any>,
	update(): Promise<any>,
	delete(): Promise<any>,
}