import type { IObserver } from "../Observer/Observer";
import type { ICar } from "./Car";

export interface ICars {
	fetchItems()          : void,
	getItems()            : ICar[],
	setItems(cars: ICar[]): void,

	createItem(item: ICar): void,
	updateItem(item: ICar): void,
	removeItem(id: string | number): void,

	sortItems(sort_option: string): void,
}