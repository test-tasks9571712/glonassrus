export interface ICar {
	id: (number | string),
	name: string,
	model: string,
	year: number,
	color: string,
	price: number,
	latitude: number,
	longitude: number,
}