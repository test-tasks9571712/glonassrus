import type { IObserver } from "./Observer";

export interface ISubject {
	registerOvserver(observer: IObserver): void,
	removeOvserver(observer: IObserver)  : void,
	notifyOvservers() : void,
}