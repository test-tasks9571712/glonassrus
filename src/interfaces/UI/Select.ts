export interface ISelectItem {
	value: string,
	text : string,
}

export interface ISelect<T> {
	items: T[],
	placeholder: string,
}