export interface IButton {
	label : string,
	color?: string,
	type? : ("button" | "submit" | "reset" | undefined)
}