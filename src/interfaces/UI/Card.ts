export interface ICard {
	id: (string | number)
}

export interface ICards<T> {
	cards: T[],
	is_buttons: boolean,
	empty_cards_label: string,
}