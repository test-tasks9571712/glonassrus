import type { ICar } from "@/interfaces/Cars/Car";

export interface IModal {
	item      : object,
	is_buttons: boolean,
	header    ?: string
}