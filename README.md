# glonassrus

This template should help get you started developing with Vue 3 in Vite.

## Задача
Создать веб-страницу с использованием фреймворком Vue или React.

1. Получить данные объектов из API путем REST-запроса. Используя данные отрисовать компоненты объектов в виде карточек с полями.

2. Реализовать сортировку объектов по значениям (год выпуска и стоимость).

3. Добавить возможность редактирования и удаления карточек на фронтенд части (изменение названия марки, модели и стоимости).

Дополнительное задание:
Реализовать отображение объектов на карте, использую их координаты. (API можно использовать любое)

Плюсом будет использование TypeScript *

P.S.Оформление UI на усмотрение

## Ссылка на превью
https://glonassrus.netlify.app/

## Project Setup

```sh
npm install
```

### Compile and Hot-Reload for Development

```sh
npm run dev
```

### Type-Check, Compile and Minify for Production

```sh
npm run build
```
